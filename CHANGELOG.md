## 0.1.0 (2021-10-24)

### Fix

- added check for keys in eventparser

### Feat

- add configuration loading, validation and generation

## 0.0.4 (2021-10-10)
